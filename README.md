# Cumulus Linux + NetQ Demo - EVPN in the DC 

This virtual network will demonstrate Cumulus Linux (VX) in a leaf-spine topology that was made with the [topology converter](https://gitlab.com/cumulus-consulting/tools/topology_converter/). This demo puts several technologies together to demo a simple EVPN deployment:

* EVPN L2VNIs
* EVPN L3VNIs
* Cumulus NetQ (needs software from Cumulus)
* Ubuntu 18.04 hosts

## Virtual Network Setup

This simulation takes advantage of [Vagrant](https://www.vagrantup.com) to spin up an entire data centre, including a Clos-based leaf / spine / exit leaf topology with dual-connected hosts via MLAG.  

### Prerequisites
------------------------
* Running this simulation without NetQ takes roughly 12G of RAM 
* Running this simulation with NetQ takes roughly 78G of RAM (NetQ requires 64G RAM, SSD and 8 CPU cores)
* Internet connectivity is required from the hypervisor. Multiple packages are installed on both the switches and servers when the lab is created.
* Setup your bare metal server and hypervisor according to the instructions with [Vagrant, Libvirt and KVM](https://docs.cumulusnetworks.com/display/VX/Vagrant+and+Libvirt+with+KVM+or+QEMU)
* For NetQ, you can get a copy of the NetQ VM from a Cumulus representative. It is not publicly available.


### Setting Up The Simulation (via Libvirt + KVM)
------------------------
1. Start the topology with the the following commands. This will run the vagrant commands to start the topology. 

```
git clone https://gitlab.com/petercrocker/cumulus-generic-setup.git
cd cumulus-generic-setup
vagrant up oob-mgmt-server oob-mgmt-switch
vagrant up leaf01 leaf02 leaf03 leaf04 spine01 spine02 exit01 exit02 server01 server02 server03 server04 edge01 internet
```

2. Enter the environment by running `vagrant ssh oob-mgmt-server`. On the oob-mgmt-server, the same git repository needs to be cloned to provide the necessary Ansible playbooks. This will run an ansible playbook that provisions the network devices and servers. 

```
vagrant ssh oob-mgmt-server
git clone https://gitlab.com/petercrocker/cumulus-generic-setup.git
cd cumulus-generic-setup/automation
ansible-playbook setup.yml
ansible-playbook config_restore.yml
```

### Setting Up NetQ
------------------------
To add on NetQ, scp the NetQ libvirt VM to your home directory on your hypervisor server, and run the following:

```vagrant box add --name cumulus/netq241 netq-2.4.1-ubuntu-18.04-ts-libvirt.box
cd cumulus-generic-setup
vagrant up netq-server
vagrant ssh oob-mgmt-server
cd cumulus-generic-setup/automation
ansible-playbook netq/netq_server_setup.yml
ansible-playbook netq/netq_agent_setup.yml
ssh leaf01
netq check agents
```

## Demonstrations
------------------------
The following demos have been listed below with sample commands:
* PTM (Prescriptive Topology Manager)
* NetQ Checks
* NetQ Trace

### Using PTM
------------------------
In data center topologies, right cabling is a time-consuming endeavor and is error prone. [Prescriptive Topology Manager](https://docs.cumulusnetworks.com/cumulus-linux/Layer-1-and-Switch-Ports/Prescriptive-Topology-Manager-PTM/) (PTM) is a dynamic cabling verification tool to help detect and eliminate such errors. It is open source. 

To check the cabling of the entire DC topology, run the following on the oob-mgmt-server:

```cd cumulus-generic-setup/automation
ansible leaf:spine:exit:internet -a "ptmctl --lldp -d"
```

### Demonstrate NetQ Checks
------------------------
1. Run the following NetQ commands on the oob-mgmt-server before you break the demo:

   `netq check bgp`

   `netq check clag`

This will show the customer the current status of the network and it should come back with the following:

   BGP:

   `Total Nodes: 8, Failed Nodes: 0, Total Sessions: 30, Failed Sessions: 0`

   CLAG:

   `Checked Nodes: 4, Failed Nodes: 0`

2. Run the following command to break the demo:

   `ansible-playbook netq/netq_break.yml`

   This will break a BGP interface on spine02 and a CLAG connection on leaf01

4. Run the following NetQ commands:

   `netq check bgp`

   `netq check clag`

You will see that there is now a broken BGP and CLAG connection:

BGP - netq check bgp

```Total Nodes: 8, Failed Nodes: 2, Total Sessions: 30 , Failed Sessions: 2,
Hostname          VRF             Peer Name         Peer Hostname     Reason                                        Last Changed
----------------- --------------- ----------------- ----------------- --------------------------------------------- -------------------------
leaf01            default         swp52             spine02           BGP session with peer spine02 (swp52 vrf defa Thu Sep 19 18:32:06 2019
                                                                      ult) failed, reason: Hold Timer Expired
spine02           default         swp1              leaf01            BGP session with peer leaf01 (swp1 vrf defaul Thu Sep 19 18:31:50 2019
                                                                      t) failed, reason: Link Admin Down
```
CLAG - netq check clag

```   Checked Nodes: 4, Warning Nodes: 2
   Hostname         Warning
   ---------------- --------------------------------------------------------------------------
   leaf01           Bond bond02 is down
   leaf02           Bond bond02 is singly connected
```

5. Run the following command to fix the demo:

   `ansible-playbook netq/netq_fix.yml`

6. Repeat the NetQ commands from Step #4:

   `netq check bgp`

   `netq check clag`

You will now see that the NetQ server is reporting that the BGP peer and CLAG connections are back up

### Demonstrate NetQ Trace
------------------------

NetQ trace from a host: `netq trace 10.1.3.105 from server01 pretty`

```cumulus@leaf01:mgmt:~$ netq trace 10.1.3.105 from server01 pretty
Number of Paths: 8
Number of Paths with Errors: 0
Number of Paths with Warnings: 2
  Path: 1 Link mtu mismatch between spine02:swp29 (9216) and exit02:swp52 (1500)
  Path: 5 Link mtu mismatch between spine02:swp29 (9216) and exit02:swp52 (1500)
Path MTU: 1500

 server01 bond0 -- swp1 <vlan13> leaf02 vni: 13 swp52 -- swp2 spine02 swp29 -- swp52 vni: 13 exit02 bond01 -- bond0 edge01
                                                swp52 -- swp2 spine02 swp30 -- swp52 vni: 13 exit01 bond01 -- bond0 edge01
          bond0 -- swp1 <vlan13> leaf02 vni: 13 swp51 -- swp2 spine01 swp29 -- swp51 vni: 13 exit02 bond01 -- bond0 edge01
                                                swp51 -- swp2 spine01 swp30 -- swp51 vni: 13 exit01 bond01 -- bond0 edge01
          bond0 -- swp1 <vlan13> leaf01 vni: 13 swp52 -- swp1 spine02 swp29 -- swp52 vni: 13 exit02 bond01 -- bond0 edge01
                                                swp52 -- swp1 spine02 swp30 -- swp52 vni: 13 exit01 bond01 -- bond0 edge01
          bond0 -- swp1 <vlan13> leaf01 vni: 13 swp51 -- swp1 spine01 swp29 -- swp51 vni: 13 exit02 bond01 -- bond0 edge01
                                                swp51 -- swp1 spine01 swp30 -- swp51 vni: 13 exit01 bond01 -- bond0 edge01
```

NetQ trace from a VRF on a switch: `netq trace 10.1.3.105 from leaf01 vrf vrf1 pretty`

```cumulus@leaf01:mgmt:~$ netq trace 10.1.3.105 from leaf01 vrf vrf1 pretty
Number of Paths: 4
Number of Paths with Errors: 0
Number of Paths with Warnings: 1
  Path: 1 Link mtu mismatch between spine02:swp29 (9216) and exit02:swp52 (1500)
Path MTU: 1500

 leaf01 <vlan4001> vni: 104001 swp52 -- swp1 spine02 swp29 -- swp52 vni: 104001 <vlan4001> exit02 <vlan13> bond01 -- bond0 edge01
                               swp52 -- swp1 spine02 swp30 -- swp52 vni: 104001 <vlan4001> exit01 <vlan13> bond01 -- bond0 edge01
 leaf01 <vlan4001> vni: 104001 swp51 -- swp1 spine01 swp29 -- swp51 vni: 104001 <vlan4001> exit02 <vlan13> bond01 -- bond0 edge01
                               swp51 -- swp1 spine01 swp30 -- swp51 vni: 104001 <vlan4001> exit01 <vlan13> bond01 -- bond0 edge01
```