# Running Batfish

This demo will run ansible playbooks to test network health via [batfish](https://www.batfish.org) 

## Setup Batfish
------------------------
Run the following:
```
cd cumulus-generic-setup/automation
ansible-playbook batfish/batfish_setup.yml
```

## Testing Batfish
------------------------
Run the following:
```
cd cumulus-generic-setup/automation
ansible-playbook batfish/batfish_test.yml
```

## Using Batfish
------------------------
Run the following:
```
cd cumulus-generic-setup/automation
ansible-playbook batfish/batfish_test.yml
```
